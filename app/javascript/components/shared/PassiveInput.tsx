import React, {
  DetailedHTMLProps,
  InputHTMLAttributes,
  useState,
} from "react";

/**
 * Simple wrapper for `<input />` where we don't care about current state,
 * but want to be able to set initial value.
 */
const PassiveInput = (
  props: DetailedHTMLProps<
    InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > & { initialValue?: string; initialChecked?: boolean }
) => {
  const { initialValue, initialChecked, ...inputProps } = props;
  const [val, setVal] = useState(props.initialValue || "");
  const [checked, setChecked] = useState(props.initialChecked || false);

  if (["checkbox", "radio"].includes(inputProps.type)) {
    return (
      <input
        checked={checked}
        onChange={() => setChecked(!checked)}
        {...inputProps}
      />
    );
  }
  return (
    <input
      value={val}
      onChange={(e) => setVal(e.target.value)}
      {...inputProps}
    />
  );
};

export default PassiveInput;
