import React, { useState } from "react";
import PassiveInput from "../shared/PassiveInput";
import Action from "./Action";

export default function Form({
  errors,
  action,
  method,
  habit,
  actions,
  authenticity_token,
}) {
  const [positive, setPositive] = useState(true);

  const onPositiveChange = (event) => {
    setPositive(event.target.value === "true");
  };

  return (
    <div className="w-full max-w-xl m-auto mt-32">
      {Object.keys(errors).length !== 0 ? JSON.stringify(errors) : null}
      <form action={action} method="post" className="card">
        {/* Add Rails-required hidden fields for 1) method (in case of PATCH) and 2) authenticity token */}
        {method ? (
          <input name="_method" type="hidden" value={method} readOnly />
        ) : null}
        <input
          type="hidden"
          name="authenticity_token"
          value={authenticity_token}
          readOnly
        />
        <h1>Habit</h1>
        <div className="w-1/3 h-px bg-gray-400 mb-4" />
        <div className="mb-4">
          <label htmlFor="name">
            I want to
            <select
              className="font-bold"
              name="habit[positive]"
              id="positive"
              onChange={onPositiveChange}
              defaultValue="true"
            >
              <option value="true">start</option>
              <option value="false">stop</option>
            </select>
            :
          </label>
          <PassiveInput
            className="text-field"
            placeholder={
              positive
                ? "Meditating every morning"
                : "Using screens after 9pm"
            }
            type="text"
            name="habit[name]"
            id="name"
            initialValue={habit.name || ""}
            required
          />
        </div>
        {positive !== null ? (
          <div>
            <h1 className="mt-4">Actions</h1>
            <div className="w-1/3 h-px bg-gray-400 mb-4" />

            {actions?.map((action) => (
              <Action key={action.key} action={action} positive={positive} />
            ))}
          </div>
        ) : null}

        <button className="btn-primary mt-4" type="submit">
          Save Habit
        </button>
      </form>
    </div>
  );
}
