import React from "react";
import PassiveInput from "../shared/PassiveInput";

const opposites = {
  obvious: "invisible",
  attractive: "unattractive",
  easy: "difficult",
  satisfying: "unsatisfying",
};

const placeholders = {
  obvious: "Set a daily reminder on my phone at 7:45am",
  attractive: "Print list of benefits and put it somewhere obvious",
  easy: "Use a guided meditation app",
  satisfying: "Reward myself with my favorite yogurt every time",
  invisible: "Move chargers across my room and use Do Not Disturb mode",
  unattractive: "Memorize health effects of lack of sleep time/quality",
  difficult: "State my reasons out loud before using a device",
  unsatisfying: "Set up a $5 donation to charity every time I fail",
};

export default function Action({
  positive,
  action,
}: {
  positive: boolean;
  action: any;
}) {
  const attribute = positive ? action.key : opposites[action.key];
  return (
    <div className="mb-4">
      <label htmlFor={`actions[${action.key}][description]`} className="ml-8">
        To make this habit <span className="uppercase">{attribute}</span>, I
        will:
      </label>
      <div className="flex">
        <PassiveInput
          className="self-center mr-4"
          name={`actions[${action.key}][done]`}
          id={`actions[${action.key}][done]`}
          type="checkbox"
          initialChecked={action.done}
        />
        <PassiveInput
          className="text-field"
          name={`actions[${action.key}][description]`}
          id={`actions[${action.key}][description]`}
          type="text"
          placeholder={`${placeholders[attribute]}`}
          initialValue={action.description}
        />
      </div>
    </div>
  );
}
