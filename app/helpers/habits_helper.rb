require 'date'

module HabitsHelper
  def days_since(datetime)
    current_date = Time.now.to_date
    dt_parsed = Date.parse(datetime.to_s)

    num_days = current_date.mjd - dt_parsed.mjd

    "#{num_days} #{'day'.pluralize(num_days)}"
  end

  def habit_type(positive)
    positive ? :Building : :Breaking
  end

  def fraction_completed(actions)
    action_done_count = 0
    nonempty_action_count = 0
    actions.each do |action|
      next if action[:description].to_s.strip.empty?

      nonempty_action_count += 1
      action_done_count += 1 if action[:done]
    end
    "#{action_done_count} / #{nonempty_action_count}"
  end
end
