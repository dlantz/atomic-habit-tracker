class HabitsController < ApplicationController
  before_action :set_habit, only: %i[show edit update destroy]

  # GET /habits
  # GET /habits.json
  def index
    @habits = Habit.all
  end

  # GET /habits/1
  # GET /habits/1.json
  def show
    @habit = Habit.find(params[:id])
  end

  # GET /habits/new
  def new
    @habit = Habit.new
    @initial_actions = %i[obvious attractive easy satisfying].map { |key| { key: key } }
  end

  # GET /habits/1/edit
  def edit; end

  # POST /habits
  # POST /habits.json
  def create
    @habit = Habit.new(habit_params)
    respond_to do |format|
      if @habit.save
        actions_hash = params[:actions]
        actions_hash&.each do |key, action|
          @habit.actions.create(key: key, description: action[:description], done: action[:done])
        end
        format.html { redirect_to habits_path, notice: 'Habit was successfully created.' }
        format.json { render :show, status: :created, location: @habit }
      else
        @habit.errors.full_messages.each do |error|
          puts error
        end
        format.html { render 'new' }
        format.json { render json: @habit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /habits/1
  def update
    respond_to do |format|
      if @habit.update(habit_params)
        # iterate through and update actions if they exist. Not super efficient
        params[:actions]&.each do |key, action|
          puts('updating action')
          action_to_update = @habit.actions.find_by(key: key)
          action_to_update.update(description: action[:description], done: action[:done])
        end
        format.html { redirect_to habits_path, notice: 'Habit was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /habits/1
  # DELETE /habits/1.json
  def destroy
    @habit.destroy
    respond_to do |format|
      format.html { redirect_to habits_url, notice: 'Habit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_habit
    @habit = Habit.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def habit_params
    params.require(:habit).permit(:name, :positive, :last_reset)
  end
end
