class LogsController < ApplicationController
  def show
    @habit = Habit.find(params[:habit_id])
    @log = @habit.logs.find(params[:id])
  end

  def new
    @habit = Habit.find(params[:habit_id])
  end

  def create
    @habit = Habit.find(params[:habit_id])
    @habit.logs.create(log_params)
    redirect_to @habit
  end

  private

  def log_params
    params.require(:log).permit(:text)
  end
end
