class Habit < ApplicationRecord
  attribute :last_reset, default: Time.now
  validates :name, presence: true
  validates :positive, inclusion: { in: [true, false] }

  has_many :actions, dependent: :destroy
  has_many :logs, dependent: :destroy
end
