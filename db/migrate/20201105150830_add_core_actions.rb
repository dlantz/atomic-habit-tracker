class AddCoreActions < ActiveRecord::Migration[6.0]
  def change
    add_column :habits, :action_obvious, :string
    add_column :habits, :action_attractive, :string
    add_column :habits, :action_easy, :string
    add_column :habits, :action_satisfying, :string
  end
end
