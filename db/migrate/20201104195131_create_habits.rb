class CreateHabits < ActiveRecord::Migration[6.0]
  def change
    create_table :habits do |t|
      t.string :name
      t.boolean :positive
      t.integer :streak

      t.timestamps
    end
  end
end
