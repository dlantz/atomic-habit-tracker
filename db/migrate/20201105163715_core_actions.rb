class CoreActions < ActiveRecord::Migration[6.0]
  def change
    rename_table :additional_actions, :actions
    remove_column :habits, :action_obvious
    remove_column :habits, :action_attractive
    remove_column :habits, :action_easy
    remove_column :habits, :action_satsifying
    add_column :actions, :key, :string
  end
end
