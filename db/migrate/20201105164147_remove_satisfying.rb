class RemoveSatisfying < ActiveRecord::Migration[6.0]
  def change
    remove_column :habits, :action_satisfying
  end
end
