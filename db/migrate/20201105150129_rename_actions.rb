class RenameActions < ActiveRecord::Migration[6.0]
  def change
    rename_table :actions, :additional_actions
  end
end
