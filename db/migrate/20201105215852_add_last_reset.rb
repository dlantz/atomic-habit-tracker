class AddLastReset < ActiveRecord::Migration[6.0]
  def change
    add_column :habits, :last_reset, :datetime
    remove_column :habits, :streak
  end
end
