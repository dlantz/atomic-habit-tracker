# Atomic Habit Tracker

A simple application to help with building & breaking habits. Inspired by James Clear's [Atomic Habits](https://jamesclear.com/atomic-habits).

## Requirements

- Ruby version 2.6.3
- Node (and yarn)

## Local Development Setup

```bash
# install dependencies
bundle install
yarn

# run migration scripts
rails db:migrate

# run rails server
rails server
```

## Testing

```bash
rails test
```
